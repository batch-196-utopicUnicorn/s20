console.log("Hello World");


function message(){
	console.log("Dahan dahan sa pagkain")
}

//while loop
//A while loop allows us to repeat a task/code while the condition is true.


/*
	while(condition){
	
		task;
		increment/decrement;
	}

*/

//example
//variable for counter, it will set how many times we will be able to repeat our task.

let count = 5;

//as long as our count variable's value is not equal to zero, we will be able to repeat our task.
/*while(count !== 0){

	message();
	count--;
}*/

/*while(count !== 0){

	console.log(count);
	count--;
}
*/
//first time loop run: count = 5 -> before we start the loop, we decrement to 4;
//first time loop run: count = 4 -> before we start the loop, we decrement to 3;
//first time loop run: count = 3 -> before we start the loop, we decrement to 2;
//first time loop run: count = 2 -> before we start the loop, we decrement to 1;
//first time loop run: count = 1 -> before we start the loop, we decrement to 0;

//On a sixth "possible" loop, is our condition still true?
//No. Because count is now 0. The loop stops.

//Do while loops
//Do while loops are similar to while loops that it allows us to repeat actions/tasks as long as the condition is true.
//However, with a do-while loop, you are able to perform a task at least once even if the condition is not true.


/*do {
	console.log(count);
	count--;
} while (count === 0)*/

/*let counterMini = 20;

while(counterMini >= 1){
	console.log(counterMini);
	counterMini--;
}*/

//For Loop
//The for loop is a more flexible version of our while and do while loops.
//In consists of 3 parts
//1. the declaration/initialization of the counter.
//2. the condition that will be evaluated to determine if the loop will continue. 
//3. the iteration or the incrementation/decrementaion need to continue and arrive at a terminating/end condition

// for(let count = 0; count <= 20; count ++){
// 	console.log(count)
// }

// for(let x = 1; x <= 10; x ++){
// 	for(let y = 10; y > 0; y--){
// 		console.log(`The sum of ${y} * ${x} = ${y * x}`)
// 	}
	
	
// }

//Continue and Break

/*
	Continue is a keyword that allows the code to go to the next loop without finishing the current code block

*/

/*for(let counter = 0; counter <= 20; counter ++){
	//if current value of counter is even
	if(counter % 2 === 0){
		continue;
		//console log is outside the if statement because the purpose of this if statement is to skip the current value.
	}*/
	//When we use the continue keyword, the code block following is disregarded
	//and the next loop is run.
	//whenever the value of counter is an even number, we skip to the next loop.
	//therefore, only odd number are shown.

	
	// console.log(counter)
	/*
		Break - allows us to end the execution of a loop.
	*/

// 	if(counter > 10){
// 		break;
// 	}

// }

for(let i = 5; i <= 100; i += 5){
	console.log(i)
}

for(let count = 0; count <= 100; count++){
	if(count % 5 === 0){
		console.log(count)
	}
}

