console.log("Hello World");


function message(){
	console.log("Dahan dahan sa pagkain")
}

//while loop
//A while loop allows us to repeat a task/code while the condition is true.


/*
	while(condition){
	
		task;
		increment/decrement;
	}

*/

//example
//variable for counter, it will set how many times we will be able to repeat our task.

let count = 5;

//as long as our count variable's value is not equal to zero, we will be able to repeat our task.
while(count !== 0){

	message();
	count--;
}